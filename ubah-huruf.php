<?php
function ubah_huruf($string){
    $huruf = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
    $wadah = "";

    for($a = 0; $a < strlen($string); $a++ ){
        for($i = 0; $i < count($huruf); $i++){
            if($huruf[$i] == $string[$a]){
                echo $huruf[$i].' => '.$huruf[$i + 1].'<br>';
                $wadah .= $huruf[$i + 1];
            }       
        }
    }

    echo $wadah;
}

// TEST CASES
echo ubah_huruf('wow');
echo "<br>";
echo "<br>"; // xpx
echo ubah_huruf('developer');
echo "<br>";
echo "<br>"; // efwfmpqfs
echo ubah_huruf('laravel');
echo "<br>";
echo "<br>"; // mbsbwfm
echo ubah_huruf('keren');
echo "<br>";
echo "<br>"; // lfsfo
echo ubah_huruf('semangat');
echo "<br>";
echo "<br>"; // tfnbohbu

?>
